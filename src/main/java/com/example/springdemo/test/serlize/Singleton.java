/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.test.serlize;

import java.io.Serializable;

/**
 * 使用双重校验锁方式实现单例
 * @author xuleyan
 * @version Singleton.java, v 0.1 2019-04-09 8:41 PM xuleyan
 */
public class Singleton implements Serializable {

    private volatile static Singleton singleton;
    private Singleton() {}
    public static Singleton getSingleton() {
        if (singleton == null) {
            synchronized (Singleton.class) {
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }

    /**
     * 定义readResolve方法，在该方法中指定要返回的对象的生成策略，就可以防止单例被破坏
     * @return
     */
    private Object readResolve() {
        return singleton;
    }
}
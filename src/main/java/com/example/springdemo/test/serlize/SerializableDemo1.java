/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.test.serlize;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author xuleyan
 * @version SerializableDemo1.java, v 0.1 2019-04-09 8:44 PM xuleyan
 */
public class SerializableDemo1 {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("tempFile"));
        outputStream.writeObject(Singleton.getSingleton());
        File file = new File("tempFile");
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
        Singleton newInstance = (Singleton) objectInputStream.readObject();
        //判断是否是同一个对象
        System.out.println(newInstance == Singleton.getSingleton());

    }
}
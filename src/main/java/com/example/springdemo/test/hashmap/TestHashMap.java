/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.test.hashmap;

import io.swagger.models.auth.In;
import sun.jvm.hotspot.utilities.RBTree;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author xuleyan
 * @version TestHashMap.java, v 0.1 2019-03-24 7:10 PM xuleyan
 */
public class TestHashMap {

    public static void main(String[] args) {

//        Map<String, String> hashMap = new TreeMap<>();
//        hashMap.put("name", "xly");
//        hashMap.put("name", "xly2");


//        RBTree rbTree = new RBTree();

        Map<Integer, Integer> hashMap2 = new HashMap<>(2);
        for (int i = 0; i < 1000; i++) {
            Double dou = Math.random() * 100;
            hashMap2.put(i, dou.intValue());
        }
        System.out.println(hashMap2);
    }
}
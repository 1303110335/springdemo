/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.test;

import com.example.springdemo.domain.User;

import java.util.Arrays;
import java.util.Date;

/**
 * @author xuleyan
 * @version TestValue.java, v 0.1 2019-03-11 8:27 AM xuleyan
 */
public class TestValue {

    public static void main(String[] args) {


        Date date = new Date();



//        Arrays.asList(null);

//        Integer.parseInt("");
//        User user = new User();
//        user.setAge(12);
//        String name = "xly";
//        user.setName(name);
//
//        change(user);
//
//        System.out.println(user);
//        System.out.println("user3:" + System.identityHashCode(user));
    }


    private static void change(User user) {
        System.out.println("user1:" + System.identityHashCode(user));
        user = new User();
        String name = "haha";
        user.setName(name);
        System.out.println(user);
        System.out.println("user2:" + System.identityHashCode(user));
    }
}
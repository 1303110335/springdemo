/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.jvm.memory.heap;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @Describe 堆溢出测试
 * @VM args: -verbose:gc -Xms20M -Xmx20M -XX:+PrintGCDetails
 * @author xuleyan
 * @version HeapOutOfMemory.java, v 0.1 2019-04-25 9:12 AM xuleyan
 */
public class HeapOutOfMemory {
    public static void main(String[] args) {
        List<TestCase> cases = new ArrayList<>();
        while (true) {
            cases.add(new TestCase());
        }
    }
}
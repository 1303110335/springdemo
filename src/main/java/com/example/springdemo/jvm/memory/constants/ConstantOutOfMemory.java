/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.jvm.memory.constants;

import java.util.ArrayList;
import java.util.List;

/**
 * 在JDK1.7 字符串常量池被从方法区拿到了堆中, 这里没有提到运行时常量池,也就是说字符串常量池被单独拿到堆,运行时常量池剩下的东西还在方法区, 也就是hotspot中的永久代
 * 在JDK1.8 hotspot移除了永久代用元空间(Metaspace)取而代之, 这时候字符串常量池还在堆, 运行时常量池还在方法区, 只不过方法区的实现从永久代变成了元空间(Metaspace) 
 * @Described：常量池内存溢出探究（堆内存）
 * @VM args : -XX:PermSize=10M -XX:MaxPermSize=10M
 * -verbose:gc -Xms10M -Xmx10M -XX:+PrintGCDetails
 * @Result java.lang.OutOfMemoryError: Java heap space
 * @author xuleyan
 * @version ConstantOutOfMemory.java, v 0.1 2019-04-25 9:00 PM xuleyan
 */
public class ConstantOutOfMemory {

    public static void main(String[] args) {

        try {
            List<String> stringList = new ArrayList<>();
            int i = 0;
            while (true) {
                stringList.add(String.valueOf(i++).intern());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
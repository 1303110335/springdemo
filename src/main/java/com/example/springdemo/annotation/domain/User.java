/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.annotation.domain;

import com.example.springdemo.annotation.Validate;

/**
 * @author xuleyan
 * @version User.java, v 0.1 2019-04-21 10:36 AM xuleyan
 */
public class User
{
    @Validate(min = 2, max = 5)
    private String name;

    @Validate(isNotNull = true, min = 1, max = 3)
    private String age;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAge()
    {
        return age;
    }

    public void setAge(String age)
    {
        this.age = age;
    }
}
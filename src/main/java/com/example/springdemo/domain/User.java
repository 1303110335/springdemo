/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xuleyan
 * @version User.java, v 0.1 2019-02-14 4:31 PM xuleyan
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = -8772420179022299451L;

    private Long id;
    private String name;
    private Integer age;
    private Integer number;
    private String play;

    public User(Integer number, String name, Integer age) {
        this.number = number;
        this.name = name;
        this.age = age;
    }

    public User(Integer number, String name, String play) {
        this.number = number;
        this.name = name;
        this.play = play;
    }
    public User() {};

}
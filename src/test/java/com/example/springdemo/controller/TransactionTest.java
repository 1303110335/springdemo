/**
 * fshows.com
 * Copyright (C) 2013-2019 All Rights Reserved.
 */
package com.example.springdemo.controller;

import com.example.springdemo.dal.fsriskmanagement.dao.ManagerDAO;
import com.example.springdemo.dal.fsriskmanagement.dataobject.ManagerDO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * @author xuleyan
 * @version TransactionTest.java, v 0.1 2019-02-16 8:14 PM xuleyan
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Transactional
public class TransactionTest {

    @Autowired
    @Qualifier("lifeCircleTransactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private ManagerDAO managerDAO;

    @Test
    public void testTransaction() {
        TransactionStatus transaction = transactionManager.getTransaction(new DefaultTransactionDefinition());

        try {
            //第一步更新
            ManagerDO managerDO1 = new ManagerDO();
            managerDO1.setToken("token first");
            managerDO1.setId(1);
            Integer updateRows = managerDAO.updateById(managerDO1);
            Assert.assertEquals(1, updateRows.intValue());

            ManagerDO managerDO = new ManagerDO();
            managerDO.setToken("tokenadsf9");
            managerDO.setId(2);
            Integer result = managerDAO.updateById(managerDO);
            Assert.assertEquals(1, result.intValue());

            transactionManager.commit(transaction);

        } catch (Exception e) {
            System.out.println("失败了");
            transactionManager.rollback(transaction);

        }
    }

    /**
     * 回滚可以使单元测试每次运行的环境独立
     */
    @Test
    @Rollback
    public void testRollback()
    {
        String token = "newToken1";
        ManagerDO managerDO = new ManagerDO();
        managerDO.setToken(token);
        managerDO.setId(2);
        Integer result = managerDAO.updateById(managerDO);
        Assert.assertEquals(1, result.intValue());

        ManagerDO manager = managerDAO.getByUsername("xly");
        System.out.printf("manager is %s", manager);
        Assert.assertEquals(token, manager.getToken());

    }
}